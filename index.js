const express = require('express');

const PORT = 3000;

const server = express();

const router = express.Router();

const movies = [{ "id": "1", "name": "El Padrino" }, { "id": 2, "name": "El Caballero Oscuro" }, { "id": 1, "name": "La La Land" }];


// server.use('/movies', (req, res) => {
//     res.send(movies);
// });

router.get('/', (req, res) => {
    res.send('Movies Page');
});

router.get('/movies/:id', (req, res) => {
    const { id } = req.params;

    if (!id) {
        res.send(moviesArray);
    } else {
        const hasMovie = movies.some((movie) => movie.id === id)
        if (hasMovie) {
            return res.send(`Yes, we have a movie with id: ${id}`);
        } else {
            return res.send(`No, we don't have a movie with id ${id}`);
        }
    }
    // Otra forma de obtener el resultado:
        // const result = movies.filter(movie => movie.id === id);
        // if(result) {
        //     return res.send(result);
        // } else {
        //     return res.send(`No, we don't have a movie with id ${id}`);
        // }
    });

router.get('/movies', (req, res) => {
    const { name } = req.query;

    if (!name) {
        res.send(moviesArray);
    } else {
        const hasMovie = movies.some((movie) => movie.name.toLowerCase() === name.toLowerCase())
        if (hasMovie) {
            return res.send(`Yes, we have a movie with the name: ${name}`);
        } else {
            return res.send(`No, we don't have a movie called ${name}`);
        }
    }

    // Otra forma de obtener el resultado:
    // const result = movies.filter(movie => movie.name.toLowerCase() === name.toLowerCase());
    // if(result) {
    //     return res.send(result)
    // } else {
    //     return res.send(`No, we don't have a movie called: ${name}`);
    // }

});


server.use('/', router);

server.listen(PORT, () => {
    console.log(`Server running in http://localhost:${PORT}`);
});


/**
    * Crea un servidor básico con Express.js
    * Instala nodemon y comprueba que se actualiza automáticamente al hacer cambios.
    * Crea una variable llamada movies que contenga mínimo tres películas con el siguiente formato:
    [{ id: 1, name: 'El Padrino' },...]
    * Crea un endpoint GET para recoger todas las películas
    * Crea un endpoint GET que use route params para obtener una película por id
    * Crea un endpoint GET que use query params para obtener una película por nombre
 */
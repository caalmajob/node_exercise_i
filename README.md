1. Crea un servidor básico con Express.js
2. Instala nodemon y comprueba que se actualiza automáticamente al hacer cambios.
3. Crea una variable llamada movies que contenga mínimo tres películas con el siguiente formato:
    [{ id: 1, name: 'El Padrino' },...]
4. Crea un endpoint GET para recoger todas las películas
5. Crea un endpoint GET que use route params para obtener una película por id
6. Crea un endpoint GET que use query params para obtener una película por nombre
